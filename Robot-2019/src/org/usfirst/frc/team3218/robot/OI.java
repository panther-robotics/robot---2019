package org.usfirst.frc.team3218.robot;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

public class OI {

	private static OI INSTANCE = new OI();
	
	XboxController xbox = new XboxController(RobotMap.xboxPort);
	
	JoystickButton aButton = new JoystickButton(xbox, 1);
	JoystickButton bButton = new JoystickButton(xbox, 2);
	JoystickButton yButton = new JoystickButton(xbox, 4);
	JoystickButton xButton = new JoystickButton(xbox, 3);
	
	
	public double getLeftJoystickY(){
		return -applyDeadband(xbox.getY(Hand.kLeft));
	}
	
	public double getLeftJoystickX(){
		return applyDeadband(xbox.getX(Hand.kLeft));
	}

	public double getRightJoystickY(){
		return applyDeadband(xbox.getY(Hand.kRight));
	}
	
	public double getRightJoystickX(){
		return applyDeadband(xbox.getX(Hand.kRight));
	}

	public static OI getInstance() {
		return INSTANCE;
	}
	
	private static double applyDeadband(double originalNumber){
		if (originalNumber<.1 && originalNumber>-.3 )
			return 0; 
		else
			return originalNumber;
	}
	
}
