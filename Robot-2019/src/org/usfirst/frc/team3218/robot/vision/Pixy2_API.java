package org.usfirst.frc.team3218.robot.vision;

public class Pixy2_API {
	
	public static final int PIXY_DEFAULT_ARGVAL = 0x80000000;
	public static final int PIXY_BUFFERSIZE = 0x104;
	public static final int PIXY_CHECKSUM_SYNC = 0xc1af;
	public static final int PIXY_NO_CHECKSUM_SYNC = 0xc1ae;
	public static final int PIXY_SEND_HEADER_SIZE = 4;
	public static final int PIXY_MAX_PROGNAME = 33;
	
	public static final int PIXY_TYPE_REQUEST_CHANGE_PROG = 0x02;
	public static final int PIXY_TYPE_REQUEST_RESOLUTION = 0x0c;
	public static final int PIXY_TYPE_RESPONSE_RESOLUTION = 0x0d;
	public static final int PIXY_TYPE_REQUEST_VERSION = 0x0e;
	public static final int PIXY_TYPE_RESPONSE_VERSION = 0x0f;
	public static final int PIXY_TYPE_RESPONSE_RESULT = 0x01;
	public static final int PIXY_TYPE_RESPONSE_ERROR = 0x03;
	public static final int PIXY_TYPE_REQUEST_BRIGHTNESS = 0x10;
	public static final int PIXY_TYPE_REQUEST_SERVO = 0x12;
	public static final int PIXY_TYPE_REQUEST_LED = 0x14;
	public static final int PIXY_TYPE_REQUEST_LAMP = 0x16;
	public static final int PIXY_TYPE_REQUEST_FPS = 0x18;
	
	public static final int PIXY_RESULT_OK = 0;
	public static final int PIXY_RESULT_ERROR = -1;
	public static final int PIXY_RESULT_BUSY = -2;
	public static final int PIXY_RESULT_CHECKSUM_ERROR = -3;
	public static final int PIXY_RESULT_TIMEOUT = -4;
	public static final int PIXY_RESULT_BUTTON_OVERRIDE = -5;
	public static final int PIXY_RESULT_PROG_CHANGING = -6;
	
	
	
}
