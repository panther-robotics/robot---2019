package org.usfirst.frc.team3218.robot.vision;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Pixy extends I2C {
	//The width parameters of the tape 
	private final int TAPE_WIDTH_MIN = 0;
	private final int TAPE_WIDTH_MAX = 0;
	//Current objective of the pixy
	private Objective OBJECTIVE;
	//The size of the incoming/outgoing packets
	private final int PACKET_SIZE = 0;
	//Packet buffers for sending and receiving
	byte[] receivingPacket = new byte[PACKET_SIZE];
	byte[] sendingPacket = new byte[PACKET_SIZE];
	//Used for setting the purpose of the pixy
	private int[] resolution = new int[2];
	public enum Objective  {
		LINE_TRACKING("LINES"),
		BLOB_RECOGNITION("BLOBS");
		
		// used to command solenoid
		public final String value;
		Objective(String value){
			this.value = value;
		}
	}
	/**
	 * Used for setting the brightness of the pixy camera.
	 */
	public enum Brightness{
		OFF(0),
		LOW(33),
		MEDIUM(66),
		HIGH(100);
		public final int value;
		Brightness(int value){
			this.value = value;
		}
	}
	
	
	public Pixy(Port port, int deviceAddress) {
		super(port, deviceAddress);
		//Checks to make sure the pixy is found at the device address
		isConnected();
		//getResolution(resolution);
	}
	
	public void setObjective(Objective intention) {
		OBJECTIVE = intention;
	}
	//Sets the objective (line tracking or blob recognition
	public Objective getObjective() {
		return OBJECTIVE;
	}
	
	private void receive() {
		//Checks again to make sure pixy is connected
		if(isConnected()) {
			
		//checks to see if it was able to read; false = success
		// If aborted, exits method
		if(readOnly(receivingPacket,PACKET_SIZE)) {
			log("aborted");
			return;}
		else
			log("received");
		
		}
	}
	
	private boolean isConnected() {
		if(addressOnly()) {
			SmartDashboard.putString(this.toString(), "Failed to find");
			return false;}
		else {
			SmartDashboard.putString(this.toString(), "Found Pixy");
			return true;}
	}
	
	private void log(String s) {
		SmartDashboard.putString(this.toString(), s);
	}
	
	/*
	 * public boolean setBrightness() {
	 */
		
	//}
	
}
