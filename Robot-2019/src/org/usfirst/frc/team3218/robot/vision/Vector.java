package org.usfirst.frc.team3218.robot.vision;

public class Vector {
	public double angle;
	public double x;
	public double y;
	
	public Vector(double angle, double x, double y) {
		this.angle = angle;
		this.x = x;
		this.y = y;
	}
}
