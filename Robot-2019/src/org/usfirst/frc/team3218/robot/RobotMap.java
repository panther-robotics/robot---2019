package org.usfirst.frc.team3218.robot;

public class RobotMap {

	//Motor Controller Ports
	public static int frontLeftMotorPort = 1;
	public static int frontRightMotorPort = 2;
	public static int backLeftMotorPort = 3;
	public static int backRightMotorPort = 4;

	// HID Ports
	public static int xboxPort = 3;
	
}


