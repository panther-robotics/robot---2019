package org.usfirst.frc.team3218.robot.drivetrain.commands;

import org.usfirst.frc.team3218.robot.OI;
import org.usfirst.frc.team3218.robot.subsystems.Drivetrain;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class DriveWithXbox extends Command {

    public DriveWithXbox() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Drivetrain.getInstance());
    	
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	System.out.println("test2");
    	System.out.println(Drivetrain.getInstance());
    	System.out.println(Drivetrain.getInstance().getInstance().getInstance());
    	SmartDashboard.putString("DriveWithXbox: ", "initialized");
    }
    
    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	
    	SmartDashboard.putString("DriveWithXbox :", "Executing");
    	
    	Drivetrain.getInstance().drive(
    			OI.getInstance().getLeftJoystickY(),
    			OI.getInstance().getLeftJoystickX(),
    			OI.getInstance().getRightJoystickX());
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    SmartDashboard.putString("DriveWithXbox :", "Ended");
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    SmartDashboard.putString("DriveWithXbox :", "Interrupted");
    }
}
