package org.usfirst.frc.team3218.robot.subsystems;

import org.usfirst.frc.team3218.robot.RobotMap;
import org.usfirst.frc.team3218.robot.drivetrain.commands.DriveWithXbox;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class Drivetrain extends Subsystem {
	
	private static Drivetrain INSTANCE = new Drivetrain();
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	
	private WPI_TalonSRX frontLeftMotor = new WPI_TalonSRX(RobotMap.frontLeftMotorPort);
	private WPI_TalonSRX frontRightMotor = new WPI_TalonSRX(RobotMap.frontRightMotorPort);
	private WPI_TalonSRX backLeftMotor = new WPI_TalonSRX(RobotMap.backLeftMotorPort);
	private WPI_TalonSRX backRightMotor = new WPI_TalonSRX(RobotMap.backRightMotorPort);
	
	private MecanumDrive robotDrive = new MecanumDrive(
			frontLeftMotor, backLeftMotor, 
			frontRightMotor, backRightMotor);
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new DriveWithXbox());
    }

    public void drive(double y, double x, double z) {
    	SmartDashboard.putNumber("Driving value y", y);
    	robotDrive.driveCartesian(y, x, z);
    }
    
    public static Drivetrain getInstance(){
    	return INSTANCE;
    }
    
}

