package org.usfirst.frc.team3218.robot.subsystems;

import org.usfirst.frc.team3218.robot.vision.Pixy;
import org.usfirst.frc.team3218.robot.vision.Pixy.Objective;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 *
 */
public class Vision extends Subsystem {
	private Pixy[] doubleton = new Pixy[2];
	private int pixyAddressOne = 0x54;
	private int pixyAddressTwo = 0x55;
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	
	public Vision() {
		doubleton[1] = new Pixy(I2C.Port.kOnboard, pixyAddressOne);
		doubleton[2] = new Pixy(I2C.Port.kOnboard, pixyAddressTwo);
		doubleton[1].setObjective(Objective.LINE_TRACKING);
		doubleton[2].setObjective(Objective.LINE_TRACKING);
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }

    public Pixy[] getPixies() {
    	return doubleton;
    }
    
}

